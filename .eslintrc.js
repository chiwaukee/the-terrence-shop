module.exports = {
  extends: "@chiwaukee",
  globals: {
    expect: "readonly",
    describe: "readonly",
    it: "readonly",
  },
};
