module.exports = {
  projects: ["./src/back/jest.config.js", "./src/front/jest.config.js"],
  collectCoverageFrom: ["**/*.{ts,tsx}", "!**/node_modules/**", "!**/*.d.ts"],
};
