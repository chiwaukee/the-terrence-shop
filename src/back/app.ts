import express from "express";
import helmet from "helmet";

import routes from "./routes";

const app = express();

app.use(
  helmet({
    crossOriginEmbedderPolicy: false,
    contentSecurityPolicy: {
      directives: {
        "default-src": "'self' *",
        "script-src": "'unsafe-inline' * 'unsafe-eval'",
        "img-src": "*",
      },
    },
  })
);
app.disable("x-powered-by");

app.use(routes);

export default app;
