import supertest from "supertest";

import app from "./app";

describe("The App", () => {
  it("should return 200", () => supertest(app).get("/health").expect(200));
});
