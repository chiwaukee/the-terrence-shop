import { BrowserRouter, Routes, Route } from "react-router-dom";
import { Page } from "./Components/Page";

export function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Page />} />
      </Routes>
    </BrowserRouter>
  );
}
