import { useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCopy, faEnvelope } from "@fortawesome/free-regular-svg-icons";
import { toast } from "bulma-toast";

import image from "../images/terrence.jpg";

export function Page() {
  useEffect(() => {
    (window as any).xProductBrowser(
      "categoriesPerRow=3",
      "views=grid(20,3) list(60) table(60)",
      "categoryView=grid",
      "searchView=list",
      "id=my-store-79513273"
    );
  }, []);

  return (
    <section className="section">
      <div className="container">
        <div className="content has-text-centered">
          <h3 className="title is-3">The Terrence Shop</h3>
        </div>
        <div id="my-store-79513273"></div>
        <div className="content has-text-centered">
          <h3 className="title is-3">Meet the Artist</h3>
        </div>
        <div
          className="card"
          style={{ margin: "0.5em", padding: "0.5rem", height: "100%" }}
        >
          <div
            className="card-image is-1x1"
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              paddingTop: "1rem",
            }}
          >
            <img
              src={image}
              alt={`image of terrence`}
              className=""
              style={{
                borderRadius: "6px",
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                width: "350px",
                height: "350px",
              }}
            />
          </div>
          <div className="card-content">
            <div className="media">
              <div className="media-content has-text-left">
                <h4 className="title is-4">Terrence</h4>
                <p className="subtitle is-6">Art Entreprenuer</p>
                <br />
                <p>
                  <span>placeholder-email@terrence.shop</span>
                  <br />
                  <a
                    title="Open your email client"
                    href="mailto:placeholder-email@terrence.shop"
                    className="has-color-primary"
                    style={{
                      display: "inline",
                      height: "100%",
                      cursor: "pointer",
                    }}
                  >
                    <FontAwesomeIcon icon={faEnvelope} />
                  </a>
                  &nbsp;&nbsp;
                  <span
                    title="Copy our email to your clipboard"
                    className="has-color-primary"
                    style={{
                      display: "inline",
                      height: "100%",
                      cursor: "pointer",
                    }}
                    onClick={() => {
                      navigator.clipboard.writeText(
                        "placeholder-email@terrence.shop"
                      );
                      toast({
                        duration: 3000,
                        message: "email address copied to clipboard",
                        type: "is-success",
                        dismissible: true,
                      });
                    }}
                  >
                    <FontAwesomeIcon icon={faCopy} />
                  </span>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
